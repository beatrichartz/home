---
title: "Test-Driven Development from the ground up"
slug: "start-test-driven-development"
date: "2021-05-30"
published: false
---

"You test. Even if you don't." - _Smart Person (not me)_

This is blog series I wish I had (in hindsight) when starting to practice Test-Driven Development [TDD] years ago. I've been practicing and teaching TDD in an Extreme Programming (XP) environment for years and had the privilege to learn from brilliant individuals. This enabled me to help a bunch of teams and engineers around the world ship better software with more peace of mind.

There is a lot of advice already out there on testing, TDD and various other adjacent topics. My intention with this series is to bring together the values, principles, practices and patterns that I have seen teams in the industry succeed with, and highlight common misconceptions and distractions. So whether you are looking to take your first steps in TDD, fill in the blanks in your practice, or are just curious for someone else's perspective, I hope there is something in here for everyone.

- What is Test-Driven Development?
- A failing test

## Research of Test-Driven Development

Research of TDD and its effects on code quality productivity and other metrics is available. A good way to dive into research are meta studies [e.g. [1](https://www.researchgate.net/publication/295863661_The_Effects_of_Test_Driven_Development_on_Internal_Quality_External_Quality_and_Productivity_A_systematic_review) or [2](https://www.researchgate.net/publication/260649027_The_Effects_of_Test-Driven_Development_on_External_Quality_and_Productivity_A_Meta-Analysis)] which will give you an overview of papers out there together with analysis. That said, almost all studies I've been able to find compare TDD to other practices, and there is almost no research available that dives deeper on the finer aspects / how to of TDD.

As a result, take my advice with a grain of salt. Just because it is written down does not make it true. The data I have is in the best case qualitative and usually anecdotal. I hope it's nevertheless a valuable perspective to add to your collection.
