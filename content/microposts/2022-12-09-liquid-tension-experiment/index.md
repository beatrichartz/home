---
date: "2022-12-09"
published: true
---

<a href="https://www.youtube.com/watch?v=OSNVo6bZGUs" target="_blank">
<img alt="Liquid Tension Experiment - Hypersonic Album Art" src="microposts/2022-12-09-liquid-tension-experiment/liquid-tension-experiment-hypersonic-album-art.webp" width="640" height="640"/>This is the kind of music that you want to put under a series of surfing wipeouts</a>. It is so. much. fun.
