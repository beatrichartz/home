---
title: "3 ways to test behaviour, not implementation"
slug: "3-ways-to-test-behaviour-not-implementation"
date: "2021-05-30"
published: false
---


**Testing behaviour, not implementation** comes up when we perceive tests or a test suite to be of low value because they are too coupled to the implementation. As a result, meaningful refactoring "in the green" / without breaking the tests is difficult.

The ability to **refactor with confidence** is one of the main reasons we write tests. It allows for continuous improvement of code to keep it ready for future change. 

So, rather than crystallizing a specific implementation,

> a test suite should allow the implementation to change within **defined boundaries of behaviour**.

## But how?

Let's imagine a simple system involving a robot gardener:

<span class="inline-block rounded-xl px-8 text-5xl align-middle">🤖</span> _This is our gardener. It takes care of plants it has been assigned._

<span class="inline-block rounded-xl px-8 text-5xl align-middle">🌿</span> 
<span class="inline-block rounded-xl pr-8 text-5xl align-middle">🌵</span> 
<span class="inline-block rounded-xl pr-8 text-5xl align-middle">🥬</span>  _There's a variety of plants in our garden with different needs._
<br/>
<br/>

Let's walk through situations that might occur when testing the gardener. There's an existing implementation that looks like this:

```ts
class RobotGardener {
  constructor(private plants: Plant[]) {}

  garden() {
    for (const plant of this.plants) {
      if (this.plantIsHydrated(plant)) continue;
      plant.water();
    }
  }

  private plantIsHydrated(plant: Plant): boolean {
    return plant.lastWaterDate.getTime() + 24 * 60 * 60 * 1000 > 
              new Date().getTime()
  }
}

interface Plant {
  lastWaterDate: Date
  water(): void
}
```

and a test:


```ts
describe('RobotGardener', () => {
  it('should water the plants when gardening', () => {
    const unhydratedPlant = {
      lastWaterDate: new Date(new Date().getTime() - 24 * 60 * 60 * 1000 - 1),
      water: jest.fn()
    };

    const hydratedPlant = {
      lastWaterDate: new Date(),
      water: jest.fn()
    };

    const gardener = new RobotGardener([unhydratedPlant, hydratedPlant]);

    gardener.garden();

    expect(unhydratedPlant.water).toHaveBeenCalledTimes(1);
    expect(hydratedPlant.water).not.toHaveBeenCalled();
  });
});
```

There are tests for various plants too, but we'll ignore them for now.

Our tests are passing. But after a few days of running the robot, our 🥬 look dry, and the 🌵 don't look happy either. Clearly, the one-size-fits-all watering schedule needs improvement.

Our plants should be on watering schedules that are appropriate for them. **Can we change our gardener implementation to make it take care of plants individually?**

_Not without breaking the tests._ But why? 

* The robot knows too much about the plants
* Plants don't know much about themselves.
* The test knows too much about how the robot works.

The keyword here is **information hiding**. The interfaces and what they hide and/or allow access to are not well defined. The boundaries of the expected behaviour are not well defined either. As a result, boundaries are fuzzy. **Fuzzy boundaries make it harder to test behaviour.**

> Tests crystallizing a specific implementation often hint to design problems

## Fixing the fuzzy boundaries

**Testing is a design activity**. Let's first think about our test design **by defining the boundaries of the behaviour our test covers**:

<span class="inline-block rounded-xl p-8 text-indigo-700 border-indigo-700 border-2">
<span class="text-5xl align-middle">🤖</span> Robot 
</span><span class="inline-block rounded-xl p-8 mx-8 text-green-700 border-green-700 border-2"><span class="text-5xl align-middle">&nbsp;</span>gardens<span class="text-5xl align-middle">&nbsp;</span></span><span class="inline-block rounded-xl p-8 text-indigo-700 border-indigo-700 border-2 border-dotted">
<span class="inline-block rounded-xl text-5xl align-middle">🌿</span> 
<span class="inline-block rounded-xl text-5xl align-middle">🌵</span> 
<span class="inline-block rounded-xl text-5xl align-middle">🥬</span>
Imaginary plants
</span>
<br/>
<br/>

_The test should cover the outcome of robot gardening in made-up gardens. The test should not integrate the behaviour of specific plants._

In other words, **we can look at the plants** we give the robot at any time and in any way we like, and **we can instruct the robot to garden** a set of plants. That's it - no peeking at how the robot executes on gardening!

Of course, there should be other tests at another level looking at the system as a whole. But for the sake of brevity, let's trust they exist, and not worry about them any further.

Instead, let's get back to our test, and, let's take a fresh look at our plant interface.

### Fixing the plant interface

We don't have real plants to test with, however we can make imaginary plants using our interface for our tests. Since we can't know how gardening is implemented, we can't just assume the gardener knows about the fake plants and which schedule to apply to watering them. **This forces a design decision for the `Plant` interface** to hide its data on watering, and instead expose a way for the robot to interact with its needs.

Prudently taking a peek at the backlog, we understand that there are other plant needs like pest control coming up. Also, our technicians are already busy installing hydration and other sensors into pots. Let's factor that into our design:

```typescript
enum PlantNeeds {
  Water
}

interface Plant {
  needs: PlantNeeds[]
  water(): void
}
```

We can now test in this way:
```typescript
describe('Robot Gardener', () => {
  it('should water the plants when gardening', () => {
    const unhydratedPlant = {
      needs: [PlantNeeds.Water],
      water: jest.fn()
    };

    const hydratedPlant = {
      needs: [],
      water: jest.fn()
    };

    const gardener = new RobotGardener([unhydratedPlant, hydratedPlant]);

    gardener.garden();

    expect(unhydratedPlant.water).toHaveBeenCalledTimes(1);
    expect(hydratedPlant.water).not.toHaveBeenCalled();
  });
});

```

and implement this:

```typescript
class RobotGardener {
  constructor(private plants: Plant[]) {}

  garden() {
    for (const plant of this.plants) {
      if (plant.needs.length === 0) continue;
      plant.water();
    }
  }
}

```

Our plants get water on their terms now.

Let's take a step back and reflect on what we just did: By **defining boundaries of the behaviour under test** we **improved the design of the implementation**.

We lean back. Our code seems extensible enough. All is well in our garden - or is it?

## Ticking boxes

As mentioned, our plants need protection from various pests, and we want our robot gardener to start deploying protectants reactively. Our technicians upgraded our plant pots with sensors detecting hydration levels and pests. Without us doing anything, plants watering schedules are now governed by their actual hydration levels and needs. Awesome! 

However, the protectants we're using discourage simultaneous watering of plants since it would minimise efficiency. Also, our pest detectors are not fool-proof, they don't detect pests well after watering. The decision is to let our robot **deploy protectants first if the plant needs it, and water the plant on the next gardening run**.

<span class="inline-block rounded-xl p-8 text-indigo-700 border-indigo-700 border-2">
<span class="text-5xl align-middle">🤖</span> Robot 
</span><span class="inline-block rounded-xl p-8 mx-8 text-green-700 border-green-700 border-2"><span class="text-5xl align-middle">&nbsp;</span>protects<span class="text-5xl align-middle">&nbsp;</span></span><span class="inline-block rounded-xl p-8 text-indigo-700 border-indigo-700 border-2 border-dotted">
<span class="inline-block rounded-xl text-5xl align-middle">🥬</span>
Plant needing protectant <i>and</i> water
</span>
<br/>
<br/>
<span class="inline-block rounded-xl p-8 text-indigo-700 border-indigo-700 border-2">
<span class="text-5xl align-middle">🤖</span> Robot 
</span><span class="inline-block rounded-xl p-8 mx-8 text-green-700 border-green-700 border-2"><span class="text-5xl align-middle">&nbsp;</span>protects<span class="text-5xl align-middle">&nbsp;</span></span><span class="inline-block rounded-xl p-8 text-indigo-700 border-indigo-700 border-2 border-dotted">
<span class="inline-block rounded-xl text-5xl align-middle">🥬</span>
Plant needing <i>only</i> protectant
</span>
<br/>
<br/>
<span class="inline-block rounded-xl p-8 text-indigo-700 border-indigo-700 border-2">
<span class="text-5xl align-middle">🤖</span> Robot 
</span><span class="inline-block rounded-xl p-8 mx-8 text-green-700 border-green-700 border-2"><span class="text-5xl align-middle">&nbsp;</span>waters<span class="text-5xl align-middle">&nbsp;</span></span><span class="inline-block rounded-xl p-8 text-indigo-700 border-indigo-700 border-2 border-dotted">
<span class="inline-block rounded-xl text-5xl align-middle">🥬</span>
Plant needing <i>only</i> water
</span>
<br/>
<br/>

Now, despite having a better interface for arguably more intelligent plants, going down the route of verifying the application of protectant but not water, or water if the plant does not need protectant, is not an exciting prospect. Thinking of the assertions:

```typescript
expect(unhydratedProtectedPlant.water).toHaveBeenCalledTimes(1);
expect(unhydratedUnprotectedPlant.water).not.toHaveBeenCalled();
expect(hydratedUnprotectedPlant.water).not.toHaveBeenCalled();

expect(unhydratedProtectedPlant.protect).not.toHaveBeenCalled();
expect(unhydratedUnprotectedPlant.protect).toHaveBeenCalledTimes(1);
expect(hydratedUnprotectedPlant.protect).toHaveBeenCalledTimes(1);
```

We could split these up into different test cases, however we would only spread the problem more. So what's the problem?
> We are **ticking boxes about how the robot behaves, rather than looking at an outcome of its behaviour**.

While there are times and places for call verifications like the ones we're doing, usually they are **limiting implementations by inadvertedly allowlisting only certain sequences of steps**.

Often, a more appropriate way to verify takes the shape of a Fake:

```typescript
enum PlantNeeds {
  Protection,
  Water
}

interface Plant {
  needs: PlantNeeds[]
  water(): void
  protect(): void
}

class FakePlant implements Plant {
  constructor(public needs: PlantNeeds[]) {}

  water() {
    if (this.hasNeed(PlantNeeds.Protection)) throw new Error('Hydrated plant in need of protection')
    if (!this.hasNeed(PlantNeeds.Water)) throw new Error('Overhydrated plant')

    this.needs = this.needsWithout(PlantNeeds.Water)
  }

  protect() {
    if (!this.hasNeed(PlantNeeds.Protection)) throw new Error('Wasted protectant')

    this.needs = this.needsWithout(PlantNeeds.Protection)
  }

  private hasNeed(needToFind: PlantNeeds): boolean {
    return this.needs.findIndex((need) => need === needToFind) >= 0
  }

  private needsWithout(needToExclude: PlantNeeds): PlantNeeds[] {
    return this.needs.filter((need) => need !== needToExclude)
  }
}
```

This allows us to write quite an expressive test demonstrating the behaviour:

```typescript
describe('Robot Gardener', () => {
  it('should be able to take care of every plant need in two gardening runs', () => {
    const plants = [
      new FakePlant([PlantNeeds.Protection, PlantNeeds.Water]),
      new FakePlant([PlantNeeds.Protection]),
      new FakePlant([PlantNeeds.Water]),
      new FakePlant([])
    ]
    const gardener = new RobotGardener(plants);

    gardener.garden();
    gardener.garden();

    expect(plants.flatMap((plant) => plant.needs)).toEqual([])
  });
});
```

And implement it:

```typescript
class RobotGardener {
  constructor(private plants: Plant[]) {}

  garden() {
    for (const plant of this.plants) {
      if (this.needs(PlantNeeds.Protection, plant)) {
        plant.protect();
        continue;
      }
      if (this.needs(PlantNeeds.Water, plant)) plant.water();
    }
  }

  private needs(needToFind: PlantNeeds, plant: Plant): boolean {
    return plant.needs.findIndex((need) => need === needToFind) >= 0
  }
}
```



- The gardeners `Plant` interface usage is removed from the test and is coincidental now. If we change the `Plant` interface, we can change the `FakePlant` in one place.
- Instead of allowing certain combinations of calls, we're denying certain call sequences now, making the test more permissive to changes overall.
- We get more expressive failures on the denied call sequences.

We also get access to a more expressive interface for behaviour changes: Let's say we find out that our robot slows down if it needs to carry both water and protectants at the same time, and we want it to do a protectant run before watering:

```typescript
describe('Robot Gardener', () => {
  it('should be able to take care of plant needs with specific gardening runs', () => {
    const plants = [
      new FakePlant([PlantNeeds.Protection, PlantNeeds.Water]),
      new FakePlant([PlantNeeds.Protection]),
      new FakePlant([PlantNeeds.Water]),
      new FakePlant([])
    ]
    const gardener = new RobotGardener(plants);

    gardener.garden(PlantNeeds.Protection);
    expect(plants.flatMap((plant) => plant.needs)).not.toContain(PlantNeeds.Protection)

    gardener.garden(PlantNeeds.Water);
    expect(plants.flatMap((plant) => plant.needs)).toEqual([])
  });
});
```

If fakes get hard to implement, it often points to the interface acquiring too many responsibilities and/or missing abstractions. As a consequence:

> Fakes can be used as canaries for design smells. 
