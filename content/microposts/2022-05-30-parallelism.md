---
date: "2022-05-30"
published: true
---

Was drumming and thinking about these <a href="https://vimeo.com/49718712" target="_blank">great explanations of</a> <a href="https://wiki.haskell.org/Parallelism_vs._Concurrency" target="_blank">Parallelism vs Concurrency</a>, <a href="https://pragprog.com/titles/pb7con/seven-concurrency-models-in-seven-weeks/" target="_blank">concurrency models</a>, etc. Music is such a great way to build an intuitive understanding for all of this. 
<br/>
<br/>
Then tried to find some research in cognitive psychology trying to understand how engineers model parallelism and concurrency in their heads. Only came up with results trying to apply parallel processing in cognitive psychology research. Oh well...