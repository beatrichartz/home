---
title: "What is Test-Driven Development?"
slug: "what-is-test-driven-development"
date: "2021-05-30"
published: false
---

## A bit of history

TDD was popularised by Extreme Programming (XP), which was developed by Kent Beck and his team when working on the Chrysler Comprehensive Compensation System (C3) project in the 1990s, and first described in the book Extreme Programming Explained in 1999. Over the years, it has found widespread adoption as a process to improve software quality and increase trust in systems.

As any other popular idea it has not come out of the blue, and also has gone through a few iterations since the late 1990s. The idea of writing expected output or behaviour before or alongside the actual implementation has been around for as long as software development existed. Unsurprisingly it exists in other fields as well: [Airbus static ground testing](https://www.youtube.com/watch?v=B74_w3Ar9nI) or smoke testing circuitry (does smoke come out if I turn this on?) which also has found its way into the nomenclature of software testing.

Over time, the idea of Behaviour Driven Development (BDD) emerged from TDD. It expanded on TDD with the idea to make human-readable business specification executable both as acceptance tests and other test cases. The most popular forms of these ideas are specifying acceptance level tests written in `Given`-`When`-`Then` format:

```gherkin
Given the user is logged in
When they click on logout
Then they are logged out and redirected to the home page
```

and test frameworks allowing for specification of test cases using `describe`, `context`, and `it`/`test` together with human-language sentences, like RSpec for Ruby, JBehave for Java, Jasmine or Jest for Javascript:

```javascript
describe('My favourite component', () => {
  it('should show a count of the number of hearts I received from my post', () => {
    // Here goes the test case written in code
  })
})
```


