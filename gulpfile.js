const { task, series, src } = require('gulp');
const requireDir = require('require-dir');

requireDir('./gulp-tasks');

task('clean', function() {
  const del = require('del');
  return del(['public/**', '!public']);
});

task('build:local', series('clean', 'theme:local', 'site:local'));
task('build:production', series('clean', 'theme:production', 'site:production'));
