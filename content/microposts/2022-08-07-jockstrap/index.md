---
date: "2022-08-07"
published: true
---

<a href="https://www.youtube.com/watch?v=FfXwm3jwVbI" target="_blank">
<img alt="Jockstrap - Concrete over Water Album Art" src="microposts/2022-08-07-jockstrap/jockstrap-concrete-over-water-album-art.webp" width="640" height="640"/>Such a great song</a>. Their album is coming out in September and 🤞 it's going to be great. Because it has to, because I ordered it.
