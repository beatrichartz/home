---
date: "2022-09-10"
published: true
---
<a href="https://wizardzines.com/" target="_blank"><img alt="Wizardzines How Containers Work Album Art" src="microposts/2022-09-11-wizardzines-questions/how-containers-work.webp" width="800" height="518" /></a>
<a href="https://wizardzines.com/" target="_blank">Wizardzines</a>, where Julia Evans shares amazing comics on topics like How Containers Work and HTTP, has expanded to include <a href="https://questions.wizardzines.com/" target="_blank">questions</a>, where you can test your knowledge acquired reading the zines with some trivia, and <a href="https://wizardzines.com/#experiments" target="_blank">experiments</a>, a collection of useful playgrounds for e.g. nginx configuration.
<br/>
<br/>
I somehow missed these awesome updates (and that there are <a href="https://wizardzines.com/zines/all-the-zines/" target="_blank">print versions of All the Zines</a>!), made my day today.