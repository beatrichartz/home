const { task, src, dest } = require('gulp');

task('theme:local', function() {
  return compileTheme().pipe(dest("themes/minimal/assets/css/"));
});

task('theme:production', function(fetch) {
  const purgecss = require("gulp-purgecss");
  const purgeHTML = require('purgecss-from-html');

  compileSite(fetch);
  const result = compileTheme().pipe(
      purgecss({
        content: ["public/**/*.html"],
        extractors: [
          {
            extractor: purgeHTML,
            extensions: ['html']
          }
        ]
      })
    )
    .pipe(dest("themes/minimal/assets/css/"));

  return result;
});

function compileTheme() {
  const atimport = require("postcss-import");
  const postcss = require("gulp-postcss");
  const tailwindcss = require("tailwindcss");

  return src("themes/minimal/dynamic/css/**/*.css")
    .pipe(postcss([
      atimport(), tailwindcss("themes/minimal/tailwind.config.js")]));
}

function compileSite(fetch) {
  const exec = require('child_process').exec;

  exec('hugo', function (err, stdout, stderr) {
    console.log(stdout);
    console.error(stderr);
    fetch(err);
  });
}
