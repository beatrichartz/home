---
date: "2022-10-12"
published: true
---

<a href="https://www.manning.com/books/the-programmers-brain" href="_blank">
<img alt="The Programmer's Brain by Felienne Hermans book cover" src="microposts/2022-10-12-programmers-brain/the-programmers-brain.webp" width="1633" height="2048"/><br>Reading The Programmer's Brain by Felienne Hermans</a> - one of the rare books that underpins recommendations with studies and solid evidence - one question that kept creeping up for me is how different processes, practices and technologies that are in use nowadays (domain-driven design, different concurrency models, async code reviews, test-driven development, pairing, story mapping) impact cognition. Intriguing to think about!
