var dnt = (navigator.doNotTrack || window.doNotTrack || navigator.msDoNotTrack);
var doNotTrack = (dnt == "1" || dnt == "yes");

if (!doNotTrack) {
	window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;
	if (window.sessionStorage) {
		var GA_SESSION_STORAGE_KEY = 'ga:clientId';
		ga('create', 'UA-149165022-1', {
			'storage': 'none',
			'clientId': sessionStorage.getItem(GA_SESSION_STORAGE_KEY)
		});
		ga(function(tracker) {
			sessionStorage.setItem(GA_SESSION_STORAGE_KEY, tracker.get('clientId'));
		});
	}
	ga('set', 'anonymizeIp', true);
	ga('send', 'pageview');
}
