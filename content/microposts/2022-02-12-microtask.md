---
date: "2022-02-12"
published: true
---

If you invoke the same sequence of commands more than twice, write a script for it.
<br>
<br>
If you click around in a browser to simulate the same sequence of events more than twice, use browser automation.
<br>
<br>
Reproduce that bug you are fixing with an automated test first.
<br>
<br>
Seriously, your time is too valuable.
