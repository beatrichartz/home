---
date: "2022-04-22"
published: true
---

<a href="https://tidal.com/browse/album/115087247" href="_blank">

<img alt="William Doyle - Your Wilderness Revisited Album Art" src="microposts/2022-04-22-doyle/william-doyle-your-wilderness-revisited-album-art.webp" width="640" height="640"/>Stumbled over this album by accident while looking for something else.</a> Sneakily got to be a favourite in just a few days.
<br>
<br>
Also, unrelated, feeling the urge to rewatch some <a href="https://www.youtube.com/watch?v=AbAal7jIWQ4">Unfinished London</a>.
