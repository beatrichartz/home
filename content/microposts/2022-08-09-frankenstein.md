---
date: "2022-08-08"
published: true
---

LinkedIn, of all places, reminded me of this gem of a quote from Alan Cooper in a foreword of <a href="https://www.oreilly.com/library/view/user-story-mapping/9781491904893" target="_blank"> User Story Mapping</a> today:

> It’s entirely reasonable that programmers build software one feature at a time. That’s a perfectly good strategy, proven over the years. What has also been proven over the years is that, when used as a method for designing the behavior and scope of a digital product, one-feature-at-a-time yields a Frankenstein monster of a program.

_proceeds to gesticulate wildly in software_
