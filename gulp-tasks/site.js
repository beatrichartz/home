const { task, src, dest, series } = require('gulp');

task('site:local', function(fetch) {
  return compileSite(fetch);
});

task('html:production', function(fetch) {
  compileSite(fetch)

  const htmlmin = require('gulp-htmlmin');

  return src(['public/**/*.html'])
    .pipe(htmlmin({
      collapseWhitespace: true,
      collapseBooleanAttributes: true,
      removeAttributeQuotes: true,
      removeRedundantAttributes: true,
      sortAttributes: true,
      sortClassName: true,
      removeComments: true
    }))
    .pipe(dest('public'));
});

task('post:images:production:reduce', function() {
    const imagemin = require('gulp-imagemin');

    return src('content/**/*.jpg')
        .pipe(imagemin())
        .pipe(dest('content'));
});

task('post:images:production:webp', function() {
    const webp = require('gulp-webp');

    return src('content/**/*.jpg')
        .pipe(webp())
        .pipe(dest('content'));
})

task('images:production', function() {
	const imagemin = require('gulp-imagemin');

	return src('public/images/*')
		.pipe(imagemin())
		.pipe(dest('public/images'));
});

task('gzip:production', function() {
  const gzip = require('gulp-gzip');

  return src(['public/**/*.html', 'public/**/*.css', 'public/**/*.js'])
    .pipe(gzip({ threshold: 1024, gzipOptions: { level: 9 } }))
    .pipe(dest('public'));
});

task('site:production', series('html:production', 'post:images:production:reduce', 'post:images:production:webp', 'images:production', 'gzip:production'));

function compileSite(fetch) {
  const exec = require('child_process').exec;

  exec('hugo', function (err, stdout, stderr) {
    console.log(stdout);
    console.error(stderr);
    fetch(err);
  });
}

